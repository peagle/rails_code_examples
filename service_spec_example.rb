require "rails_helper"

describe "use stratus" do

  let(:company) { FactoryBot.create(:main_address) }
  let(:action)  { UsesStratus.new(company.id) }

  around(:each) do |spec|
    WebMock.disable_net_connect!
    spec.run
    WebMock.allow_net_connect!
  end

  # --------------------------------
  # GET GREET
  it "allows to get greet" do    
    stub_request(:get, "https://api.emea.fedex.com/stratus/v1/greet").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer ', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "body", headers: {})

    request = action.get_greet
    expect(request.body).to_not be_nil
  end

  # --------------------------------
  # POST TOKEN
  it "allows to post request for token" do
    stub_request(:post, "https://api.emea.fedex.com/auth/oauth/v2/token?grant_type=client_credentials&scope=oob").
         with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic ZWQxYmY4OGYtNzJiNy00MDRiLWE2NmUtYWVlYTRlZmVjZTM2OmQ5NjVjNTdhLTllYTYtNDA3Mi04MjdlLTNjNzE0NTE4NmM1Mg==', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})

    response = action.post_token
    expect(response.code).to eq("200")
  end  

  # --------------------------------
  # PARSE RESPONSE
  it "allows to json parse response" do
    raw  = {name: "Beata"}
    json_file = raw.to_json
    presponse = action.parse_response(json_file)
    expect(presponse["name"]).to eq("Beata")
  end

  # --------------------------------
  # SAVE TOKEN IN COMPANY
  it "allows to save token in company" do    
    raw = {access_token: "adef123"}
    json = raw.to_json
    action.save_token(json)
    expect(MainAddress.find(company.id).stratus_token).to eq("adef123")
  end
  
  # --------------------------------
  # CHECK CODE 200
  it "returns Ok if response code is 200" do
    stub_request(:get, "https://api.emea.fedex.com/stratus/v1/greet").
        with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer ', 'User-Agent'=>'Ruby'}).
        to_return(status: 200, body: "", headers: {})    
    expect(action.check_token).to eq("Ok")
  end


  # --------------------------------
  # CHECK CODE DIFFERENT THAN 200
  it "returns code if response code is different than 200" do
    stub_request(:get, "https://api.emea.fedex.com/stratus/v1/greet").
        with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer ', 'User-Agent'=>'Ruby'}).
        to_return(status: 401, body: "", headers: {})    
    expect(action.check_token).to eq("401")
  end

  # --------------------------------
  # RETURNS OK IF CHECK_TOKEN IS OK
  it "returns Ok if check_token is ok" do
    stub_request(:get, "https://api.emea.fedex.com/stratus/v1/greet").
        with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer ', 'User-Agent'=>'Ruby'}).
        to_return(status: 200, body: "", headers: {})    

    expect(action.ready_token).to eq("Ok")
  end

  # --------------------------------
  # IF CODE IS DIFFERENT THAN OK ASK FOR TOKEN AGAIN
  it "if code is diffent than ok ask for token again" do
    stub_request(:get, "https://api.emea.fedex.com/stratus/v1/greet").
    with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer ', 'User-Agent'=>'Ruby'}).
    to_return(status: 401, body: "", headers: {})

    stub_request(:post, "https://api.emea.fedex.com/auth/oauth/v2/token?grant_type=client_credentials&scope=oob").
      with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Basic ZWQxYmY4OGYtNzJiNy00MDRiLWE2NmUtYWVlYTRlZmVjZTM2OmQ5NjVjNTdhLTllYTYtNDA3Mi04MjdlLTNjNzE0NTE4NmM1Mg==', 'User-Agent'=>'Ruby'}).
      to_return(status: 200, body: "{\r\n  \"access_token\":\"bdfdd060-4bab-459c-a1f4-4bd0354ceebb\"}", headers: {})
   
    stub_request(:get, "https://api.emea.fedex.com/stratus/v1/greet").
      with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer bdfdd060-4bab-459c-a1f4-4bd0354ceebb', 'User-Agent'=>'Ruby'}).
      to_return(status: 200, body: "", headers: {})

    expect(action.ready_token).to eq("Ok")
  end

  # --------------------------------
  # SEND SHIPMENT REQUEST
  it "allows to send shipment request" do
    raw = {access_token: "adef123"}
    json = raw.to_json

    stub_request(:post, "https://api.emea.fedex.com/stratus/v1/shipments").
         with(body: "{\"access_token\":\"adef123\"}",
              headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer ', 'Content-Type'=>'application/json', 'User-Agent'=>'Ruby'}).
         to_return(status: 200, body: "", headers: {})
   

    response = action.send_shipment_request(json)
    expect(response.code).to eq "200"
  end

  
end