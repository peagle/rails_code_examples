$(function(){
  blank_input_field("#search_start_time");

  // ---------- SEARCH SERIALIZATION
  $("#search_invoice_number input").keyup(function() {
    $.get($("#search_invoice_number").attr("action"), $("#search_invoice_number").serialize(), null, "script");
    return false;
  });

  // ---------- DATE PICKER START AND END DATE
  $("#search_start_time").datepicker({                
    dateFormat: 'yy-mm-dd',
    onClose: function(dateText, inst) { $(inst.input).change().focusout(); }
  });   

  // ---------- PAGINATION AND SORTABLE
  $(document).on("click", ".pagination a, #invoices th a", function(event){    
    $.getScript(this.href);
    event.preventDefault();
  }); 

  // ---------- PRINT BATCH BUTTON REAL TIME PARAMS
  $(document).on('click', 'a#print_batch_pdf', function(e){    
    $(this).attr("href","/invoices?start_time=" + encodeURIComponent( $("#search_start_time").val() )
                                   + "&end_time=" + encodeURIComponent( $("#search_end_time").val() ) 
                                   + "&search=" + encodeURIComponent( $("#search").val() ) 
                                   + "&search_by_so_id=" + encodeURIComponent( $("#search_by_so_id").val() ) 
                                   + "&search_by_ref=" + encodeURIComponent( $("#search_by_ref").val() ) 
                                   + "&search_by_customer=" + encodeURIComponent( $("#search_by_customer").val() ) 
                                   + "&format=pdf" 
    );
  })  


  $(document).on('cocoon:after-insert', '#credit_lines', function(e, added_line) {    
    // ----- FIND SELECT LINE AND CHOSEN
    var select_line = added_line.find("select");
    $(select_line).chosen();  
    button_blank_select_chosen_field(select_line);  
    
    allocation_id=$("option:selected",select_line).val();

    $.ajax({
      url: "/invoices/allocation_info",
      type: "GET",
      dataType: "json",
      data: { allocation_id: allocation_id }, 
      complete: function() {},
      success: function(data, textStatus, xhr) {
        
        // ----- ADD CREDIT LINE NUMBERS          
        added_line.find(".inv_qty").html(data.qty);      
        added_line.find("input[id*='qty']").val(data.qty);      
        added_line.find(".cl_units").html(data.units);      
        added_line.find(".cl_unit_price").html(data.unit_price);      
        added_line.find(".cl_value").html(data.value);      
        added_line.find(".cl_vat").html(data.vat);      

        line_gross = (parseFloat(data.gross)).toFixed(2);          
        added_line.find(".cl_gross").html((line_gross));                                
        // ----- ADD HTML5 MAX TO LINE QTY
        added_line.find("input[id*='qty']").attr('max', data.qty);
        added_line.find("input[id*='qty']").attr('readonly', false);
        
        // ----- SET TOTAL NUMBERS
        set_cn_totals();          

      },
      error: function() {
        alert("Ajax error!")
      }
    });//end ajax         


  })//END SELECT ALLOCATION
}//END DOM

var set_cn_totals = function() {
  var lines_val     = 0.0;
  var transport_val = 0.0;
  var sundry_val    = 0.0;  
  var total_val     = 0.0;
  var total_vat     = 0.0;
  var total_gross   = 0.0;

  $(".cl_value").each(function( index, val ) {
    lines_val += parseFloat($(val).text());
  });

  if($("#invoice_credit_transport").is(':checked')){
    transport_val += parseFloat($("#invoice_credit_transport_cost").val());
  }

  if($("#invoice_credit_sundry").is(':checked')){
    sundry_val += parseFloat($("#invoice_sundry_value").val());
  }

  total_val = parseFloat(lines_val + transport_val + sundry_val).toFixed(2);  
  total_gross = (parseFloat(total_val) + parseFloat(total_val * gon.percentage)).toFixed(2);

  $("#cn_total_value").text(total_val);    
  $("#cn_gross").text(total_gross);
  
  $("#hidden_credit_value").val(total_gross);     
}//END FUN
