class PriceList < ApplicationRecord
  
  include AddTraceability
  
  # ------------------------------
  has_many   :customers
  belongs_to :customer, class_name: "Customer", :foreign_key => "customer_id"
  belongs_to :currency

  has_many :collection_price_lists
  has_many :collections, through: :collection_price_lists

  has_many :design_price_lists
  has_many :designs, through: :design_price_lists

  has_many :product_price_lists
  has_many :products, through: :product_price_lists

  accepts_nested_attributes_for :collection_price_lists, 
                                allow_destroy: true,
                                reject_if:     :all_blank

  accepts_nested_attributes_for :design_price_lists, 
                                allow_destroy: true,
                                reject_if:     :all_blank


  accepts_nested_attributes_for :product_price_lists, 
                                allow_destroy: true,
                                reject_if:     :all_blank
  
  # ------------------------------
  validates :name, presence: true, length: {maximum: 50}, uniqueness: true
  # validates :currency_id, presence: true  
  
  # ------------------------------
  def self.general_price_lists
    where(general: true)
  end
  
  # --------------------------
  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |price_list|
        csv << price_list.attributes.values_at(*column_names)
      end
    end
  end

  # --------------------------
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|      
      unless find_by_id(row["id"])
        PriceList.create! row.to_hash
      end
    end
  end


  private
  # ------------------------------
  def self.search(letter, sort_by)
    if letter
      if sort_by == "name"      
        where("LOWER(name) LIKE ?", "%#{letter.downcase}%" )  
      else        
        joins(:customer).where("LOWER(customers.name) LIKE ?", "%#{letter.downcase}%")      
      end  
    else
        all
    end
  end

  # ------------------------------
  scope :sort_by_name, -> {(with_design.order('designs.name'))}
  scope :with_design, -> {eager_load(:design)}

end
